# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0005_sem_test_fake'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='M2M',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='sem',
            name='for_key',
            field=models.ForeignKey(default=1, to='student.ForKey'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sem',
            name='m2m',
            field=models.ManyToManyField(to='student.M2M'),
        ),
    ]
