# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('f_name', models.CharField(max_length=100)),
                ('l_name', models.CharField(max_length=100)),
                ('college', models.CharField(max_length=100)),
                ('degree', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('team', models.CharField(max_length=100)),
            ],
        ),
    ]
