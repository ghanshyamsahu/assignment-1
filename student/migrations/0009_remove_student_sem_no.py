# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0008_remove_student_created_on'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='sem_no',
        ),
    ]
