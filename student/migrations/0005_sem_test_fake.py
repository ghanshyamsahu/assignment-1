# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0004_remove_sem_test_fake'),
    ]

    operations = [
        migrations.AddField(
            model_name='sem',
            name='test_fake',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
