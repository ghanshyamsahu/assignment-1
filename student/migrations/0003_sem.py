# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0002_remove_student_team'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sem_no', models.IntegerField()),
                ('name', models.CharField(max_length=255)),
                ('active', models.BooleanField(default=True)),
                ('sss', models.CharField(max_length=255, null=True, blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('test_fake', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
    ]
