# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0006_auto_20180709_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='student',
            name='sem_no',
            field=models.IntegerField(default=1),
        ),
    ]
