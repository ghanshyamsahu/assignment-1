# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0007_auto_20180709_1639'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='created_on',
        ),
    ]
