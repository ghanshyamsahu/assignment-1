# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0003_sem'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sem',
            name='test_fake',
        ),
    ]
