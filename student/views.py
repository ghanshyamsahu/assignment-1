
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.http import QueryDict
from django.views.generic import View
from django.http import HttpResponse
from .models import Student
from django.db import IntegrityError



#  class to get two parameter p1 and p2 and do the addition
class TestSum(View):

    def get(self, request):
        try:
            p1 = request.GET['p1']
            p2 = request.GET['p2']
            result = int(p1) + int(p2)
            return HttpResponse('sum is : ' + str(result), status=200)
        except Exception as exception:
            return HttpResponse("Internal Server Error", status=500)



# class to get ,update ,put and delete from student Model
class Student_handle(View):
    #Method to get the all student data
    def get(self,request):
        try:
            student_list = Student.objects.all()
            html=[]
            html.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' % (
            'First name', 'Last name', 'College', 'Degree', 'Email'))
            for student in student_list:
                html.append('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>' %(student.f_name,student.l_name,student.college,student.degree,student.email))

            return HttpResponse('<table>%s</table>' % '\n'.join(html),status=200)


        except Exception as exception:
            return HttpResponse('Internel error', status=500)



    # Method for new entry in Student model
    def post(self,request):
        try:
            post_req = request.POST

            f_name_=post_req['f_name']
            l_name_=post_req['l_name']
            college_=post_req['college']
            email_=post_req['email']
            degree_=post_req['degree']
            
            obj = Student(f_name=f_name_, l_name=l_name_,college=college_, email=email_, degree=degree_)
            obj.save()
            return HttpResponse(str(f_name_)+" record  sucessfully created ", status=200)

        except MultiValueDictKeyError:
            return HttpResponse("Multi Valued DictKey error", status=500)
        except IntegrityError:
            return HttpResponse("Integrity Error", status=500)

    #Method to delete record

    def delete(self,request,id):
        try:
            Student.objects.get(id=id).delete()
            return HttpResponse("sucessfully deleted", status=200)

        except ObjectDoesNotExist:
            return HttpResponse(' Object doesnot exist', status=500)

        except MultiValueDictKeyError:
            return HttpResponse(' Multi Value Dict Key error', status=500)



    #method to update record
    def put(self,request ,id):
        try:
            put_req=QueryDict(request.body)
            obj=Student.objects.get(id=id)

            if 'f_name' in put_req.keys():
                obj.f_name = put_req['f_name']
            if 'l_name' in put_req.keys():
                obj.l_name = put_req['l_name']
            if 'email' in put_req.keys():
                obj.email=put_req['email']
            if 'college' in put_req.keys():
                obj.college = put_req['college']
            if 'degree' in put_req.keys():
                obj.degree = put_req['degree']

            obj.save()
            return HttpResponse("sucessfully Updated ", status=200)
        except ObjectDoesNotExist:
            return HttpResponse(' Object doesnot exist', status=500)

