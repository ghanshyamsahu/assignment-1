from __future__ import unicode_literals

from django.db import models

# Student model


class Student(models.Model):

    f_name = models.CharField(max_length=100)
    l_name=models.CharField(max_length=100)
    college=models.CharField(max_length=100)
    degree=models.CharField(max_length=100)
    email=models.CharField(max_length=100)




    def __unicode__(self):
        return str(self.f_name) + ' ' + str(self.l_name) + ' ' + str(self.college)+ ' ' + str(self.degree)+ ' ' + str(self.email)




class ForKey(models.Model):

    name = models.CharField(max_length=255)

    def __unicode__(self):
        return str(self.name)


class M2M(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return str(self.name)


class Sem(models.Model):
    sem_no = models.IntegerField()
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    sss = models.CharField(null=True, blank=True, max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    test_fake = models.CharField(null=True, blank=True, max_length=255)
    for_key = models.ForeignKey(ForKey)
    m2m = models.ManyToManyField(M2M)

    def __unicode__(self):
        return str(self.sem_no) + ' ' + str(self.name) + ' ' + str(self.sss)



