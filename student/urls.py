from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import TestSum,Student_handle


urlpatterns = [
	url(r'^sum/$', csrf_exempt(TestSum.as_view()),
		name="intern-sum"),
	url(r'^student/$', csrf_exempt(Student_handle.as_view()),
		name="intern-student"),
	url(r'^student/(?P<id>\d+)/$', csrf_exempt(Student_handle.as_view()),
		name="intern-student"),

]