from django.contrib import admin

# Register your models here.
from django.contrib import admin

from models import Student,Sem,M2M, ForKey
# Register your models here.
admin.site.register(Student)
admin.site.register(Sem)
admin.site.register(M2M)
admin.site.register(ForKey)

