# Student Model Assignment
```
This project Fetch,Insert ,Update  and Delete The student record with GET,POST,PUT ,DELETE request respectively
```
## Getting Started
Cloning a  repository
    to clone the repository in your local system run 
    ```
    git clone https://ghanshyamsahu@bitbucket.org/ghanshyamsahu/assignment-1.git
    ```


### Prerequisites

```
Djnago 1.8
sqlite 
```


### Installing




```
pip install django==1.8
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests
1. run the command - python manage.py runserver 
2. go to adress http://127.0.0.1:8000/admin/ to access admin
    - username- ghanshyamsahu
    - password -0000
3. feilds in Student Model
    - f_name
    - l_name
    - college
    - degree
    - email 
4. to Insert the detail hit the POST Request at http://127.0.0.1:8000/intern/student/
5. to Fetch  detail hit the GET Request at http://127.0.0.1:8000/intern/student/
6. to update  detail hit the PUT Request at http://127.0.0.1:8000/intern/student/7/
        { 7 is here ID number ,use the id number you wish to Update anf fill the details in body of request}
7. to delete the detail hit the DELETE Request at http://127.0.0.1:8000/intern/student/7/
        { 7 is here ID number ,use the id number you wish to Update anf fill the details in body of request}



